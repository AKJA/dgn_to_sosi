﻿namespace SOSI_Converter
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Txt_TEGNSETT = new System.Windows.Forms.TextBox();
            this.Txt_KOORDSYS = new System.Windows.Forms.TextBox();
            this.Txt_ENHET = new System.Windows.Forms.TextBox();
            this.Txt_SOSI_NIVA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LBL_PRODUKTSPEK = new System.Windows.Forms.Label();
            this.Txt_PRODUSENT = new System.Windows.Forms.TextBox();
            this.Txt_EIER = new System.Windows.Forms.TextBox();
            this.Txt_SOSI_VERSJON = new System.Windows.Forms.TextBox();
            this.Txt_PRODUKTSPEK = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_REGISTRERINGSVERSJON = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.combox_FKB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button11 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.button13 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBox_UTF_Type = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.progressBar8 = new System.Windows.Forms.ProgressBar();
            this.progressBar7 = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.progressBar6 = new System.Windows.Forms.ProgressBar();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Txt_REGISTRERINGSVERSJON_new = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.combox_FKB_new = new System.Windows.Forms.ComboBox();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.button18 = new System.Windows.Forms.Button();
            this.progressBar5 = new System.Windows.Forms.ProgressBar();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button29 = new System.Windows.Forms.Button();
            this.Validation6 = new System.Windows.Forms.RadioButton();
            this.progressBar9 = new System.Windows.Forms.ProgressBar();
            this.label24 = new System.Windows.Forms.Label();
            this.Validation5 = new System.Windows.Forms.RadioButton();
            this.Validation4 = new System.Windows.Forms.RadioButton();
            this.StartValidatin = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.Validation3 = new System.Windows.Forms.RadioButton();
            this.Validation2 = new System.Windows.Forms.RadioButton();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.Validation1 = new System.Windows.Forms.RadioButton();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.progressBar12 = new System.Windows.Forms.ProgressBar();
            this.OrthoSurface = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.progressBar11 = new System.Windows.Forms.ProgressBar();
            this.button37 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.progressBar10 = new System.Windows.Forms.ProgressBar();
            this.button33 = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.Ipbox8 = new System.Windows.Forms.TextBox();
            this.Ipbox7 = new System.Windows.Forms.TextBox();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.Ipbox6 = new System.Windows.Forms.TextBox();
            this.Ipbox2 = new System.Windows.Forms.TextBox();
            this.Ipbox1 = new System.Windows.Forms.TextBox();
            this.Ipbox5 = new System.Windows.Forms.TextBox();
            this.Ipbox4 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Ipbox3 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.button24 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.checkBox_KPBuilding1 = new System.Windows.Forms.CheckBox();
            this.checkBox_KPBuilding2 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(11, 228);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Create SOSI File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1111, 383);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(176, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1011, 387);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select DGN file";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(170, 11);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(148, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select Output Folder";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1293, 381);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Browse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(326, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Browse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Txt_TEGNSETT
            // 
            this.Txt_TEGNSETT.Location = new System.Drawing.Point(603, 334);
            this.Txt_TEGNSETT.Name = "Txt_TEGNSETT";
            this.Txt_TEGNSETT.Size = new System.Drawing.Size(118, 20);
            this.Txt_TEGNSETT.TabIndex = 7;
            this.Txt_TEGNSETT.Text = "ISO8859-10";
            // 
            // Txt_KOORDSYS
            // 
            this.Txt_KOORDSYS.Location = new System.Drawing.Point(102, 21);
            this.Txt_KOORDSYS.Name = "Txt_KOORDSYS";
            this.Txt_KOORDSYS.Size = new System.Drawing.Size(88, 20);
            this.Txt_KOORDSYS.TabIndex = 8;
            this.Txt_KOORDSYS.Text = "22";
            // 
            // Txt_ENHET
            // 
            this.Txt_ENHET.Location = new System.Drawing.Point(1331, 191);
            this.Txt_ENHET.Name = "Txt_ENHET";
            this.Txt_ENHET.Size = new System.Drawing.Size(118, 20);
            this.Txt_ENHET.TabIndex = 10;
            this.Txt_ENHET.Text = "0.01";
            // 
            // Txt_SOSI_NIVA
            // 
            this.Txt_SOSI_NIVA.Location = new System.Drawing.Point(102, 46);
            this.Txt_SOSI_NIVA.Name = "Txt_SOSI_NIVA";
            this.Txt_SOSI_NIVA.Size = new System.Drawing.Size(88, 20);
            this.Txt_SOSI_NIVA.TabIndex = 11;
            this.Txt_SOSI_NIVA.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(443, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "TEGNSETT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "KOORDSYS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1272, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "ENHET";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(201, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "SOSI-VERSJON";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "SOSI-NIVÅ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(201, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "PRODUSENT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "EIER";
            // 
            // LBL_PRODUKTSPEK
            // 
            this.LBL_PRODUKTSPEK.AutoSize = true;
            this.LBL_PRODUKTSPEK.Location = new System.Drawing.Point(5, 101);
            this.LBL_PRODUKTSPEK.Name = "LBL_PRODUKTSPEK";
            this.LBL_PRODUKTSPEK.Size = new System.Drawing.Size(88, 13);
            this.LBL_PRODUKTSPEK.TabIndex = 20;
            this.LBL_PRODUKTSPEK.Text = "PRODUKTSPEK";
            // 
            // Txt_PRODUSENT
            // 
            this.Txt_PRODUSENT.Location = new System.Drawing.Point(304, 46);
            this.Txt_PRODUSENT.Name = "Txt_PRODUSENT";
            this.Txt_PRODUSENT.Size = new System.Drawing.Size(71, 20);
            this.Txt_PRODUSENT.TabIndex = 21;
            this.Txt_PRODUSENT.Text = "COWI AS";
            // 
            // Txt_EIER
            // 
            this.Txt_EIER.Location = new System.Drawing.Point(102, 72);
            this.Txt_EIER.Name = "Txt_EIER";
            this.Txt_EIER.Size = new System.Drawing.Size(88, 20);
            this.Txt_EIER.TabIndex = 22;
            this.Txt_EIER.Text = "GEOVEKST";
            // 
            // Txt_SOSI_VERSJON
            // 
            this.Txt_SOSI_VERSJON.Location = new System.Drawing.Point(304, 21);
            this.Txt_SOSI_VERSJON.Name = "Txt_SOSI_VERSJON";
            this.Txt_SOSI_VERSJON.Size = new System.Drawing.Size(71, 20);
            this.Txt_SOSI_VERSJON.TabIndex = 23;
            this.Txt_SOSI_VERSJON.Text = "4.0";
            // 
            // Txt_PRODUKTSPEK
            // 
            this.Txt_PRODUKTSPEK.Location = new System.Drawing.Point(102, 98);
            this.Txt_PRODUKTSPEK.Name = "Txt_PRODUKTSPEK";
            this.Txt_PRODUKTSPEK.Size = new System.Drawing.Size(88, 20);
            this.Txt_PRODUKTSPEK.TabIndex = 24;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Txt_PRODUKTSPEK);
            this.groupBox1.Controls.Add(this.Txt_SOSI_VERSJON);
            this.groupBox1.Controls.Add(this.Txt_KOORDSYS);
            this.groupBox1.Controls.Add(this.Txt_EIER);
            this.groupBox1.Controls.Add(this.Txt_PRODUSENT);
            this.groupBox1.Controls.Add(this.LBL_PRODUKTSPEK);
            this.groupBox1.Controls.Add(this.Txt_SOSI_NIVA);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 131);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "2:  SOSI Header Input";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(462, 113);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(233, 17);
            this.checkBox1.TabIndex = 41;
            this.checkBox1.Text = "Update Mapping  [Take Header From MDB]";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "FKB Standard";
            // 
            // Txt_REGISTRERINGSVERSJON
            // 
            this.Txt_REGISTRERINGSVERSJON.Location = new System.Drawing.Point(170, 66);
            this.Txt_REGISTRERINGSVERSJON.Name = "Txt_REGISTRERINGSVERSJON";
            this.Txt_REGISTRERINGSVERSJON.Size = new System.Drawing.Size(149, 20);
            this.Txt_REGISTRERINGSVERSJON.TabIndex = 30;
            this.Txt_REGISTRERINGSVERSJON.Text = "FKB 4.02";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Registration version";
            // 
            // combox_FKB
            // 
            this.combox_FKB.FormattingEnabled = true;
            this.combox_FKB.Items.AddRange(new object[] {
            "FKB-A",
            "FKB-B",
            "FKB-C"});
            this.combox_FKB.Location = new System.Drawing.Point(170, 40);
            this.combox_FKB.Name = "combox_FKB";
            this.combox_FKB.Size = new System.Drawing.Size(149, 21);
            this.combox_FKB.TabIndex = 33;
            this.combox_FKB.Text = "FKB-A";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Select sosi for Date of Flying";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(327, 92);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 23);
            this.button4.TabIndex = 35;
            this.button4.Text = "Browse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(170, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(149, 20);
            this.textBox3.TabIndex = 34;
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(7, 225);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(167, 23);
            this.button14.TabIndex = 38;
            this.button14.Text = "Create SOSI file";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 209);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Status:";
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(8, 173);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(369, 16);
            this.progressBar3.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar3.TabIndex = 36;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(1146, 50);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(340, 108);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Surface creaction";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(47, 69);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(164, 23);
            this.button7.TabIndex = 40;
            this.button7.Text = "create Surface_from sosi point";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(265, 54);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 23);
            this.button6.TabIndex = 39;
            this.button6.Text = "Browse";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(139, 26);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(176, 20);
            this.textBox4.TabIndex = 38;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Select sosi file for surface";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Status:";
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(9, 35);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(369, 10);
            this.progressBar2.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar2.TabIndex = 44;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(9, 19);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(369, 10);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 43;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(10, 73);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(164, 23);
            this.button11.TabIndex = 42;
            this.button11.Text = "Assign Surface Attribute";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(1406, 460);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(66, 23);
            this.button8.TabIndex = 39;
            this.button8.Text = "Browse";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(1213, 460);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(189, 20);
            this.textBox5.TabIndex = 38;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1124, 468);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "Select DGN File";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(1138, 229);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(348, 96);
            this.richTextBox1.TabIndex = 44;
            this.richTextBox1.Text = "";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(462, 145);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(159, 23);
            this.button12.TabIndex = 43;
            this.button12.Text = "Assigne New SOSI ID";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(462, 262);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(145, 23);
            this.button10.TabIndex = 42;
            this.button10.Text = "Assigne SOSI ID";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(136, 22);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(185, 20);
            this.textBox6.TabIndex = 41;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(122, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "Select Original SOSI File";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(329, 20);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 23);
            this.button9.TabIndex = 39;
            this.button9.Text = "Browse";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1178, 188);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 40;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(10, 73);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(164, 23);
            this.button13.TabIndex = 44;
            this.button13.Text = "Assign Surface Attribute";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(462, 34);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(91, 17);
            this.radioButton1.TabIndex = 46;
            this.radioButton1.Text = "New Mapping";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(462, 73);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(104, 17);
            this.radioButton2.TabIndex = 47;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Update Mapping";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(10, 48);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(370, 15);
            this.progressBar4.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar4.TabIndex = 49;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(6, -4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(413, 541);
            this.tabControl2.TabIndex = 51;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(405, 515);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "UPDATE Mapping";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox_KPBuilding1);
            this.groupBox10.Controls.Add(this.checkBox2);
            this.groupBox10.Controls.Add(this.textBox12);
            this.groupBox10.Controls.Add(this.dateTimePicker1);
            this.groupBox10.Controls.Add(this.label38);
            this.groupBox10.Controls.Add(this.label2);
            this.groupBox10.Controls.Add(this.textBox3);
            this.groupBox10.Controls.Add(this.button3);
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.button14);
            this.groupBox10.Controls.Add(this.button4);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.combox_FKB);
            this.groupBox10.Controls.Add(this.progressBar3);
            this.groupBox10.Controls.Add(this.textBox2);
            this.groupBox10.Controls.Add(this.Txt_REGISTRERINGSVERSJON);
            this.groupBox10.Controls.Add(this.label13);
            this.groupBox10.Location = new System.Drawing.Point(12, 255);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(382, 254);
            this.groupBox10.TabIndex = 53;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "3";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(12, 124);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(119, 17);
            this.checkBox2.TabIndex = 42;
            this.checkBox2.Text = "Enter Date of Flying";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(170, 147);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(148, 20);
            this.textBox12.TabIndex = 41;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(170, 121);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(206, 20);
            this.dateTimePicker1.TabIndex = 40;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(11, 129);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(0, 13);
            this.label38.TabIndex = 39;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.progressBar1);
            this.groupBox9.Controls.Add(this.progressBar2);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.button13);
            this.groupBox9.Location = new System.Drawing.Point(12, 144);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(385, 105);
            this.groupBox9.TabIndex = 52;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "2";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBox_UTF_Type);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.button9);
            this.groupBox8.Controls.Add(this.textBox6);
            this.groupBox8.Controls.Add(this.progressBar4);
            this.groupBox8.Controls.Add(this.button15);
            this.groupBox8.Location = new System.Drawing.Point(12, 10);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(385, 128);
            this.groupBox8.TabIndex = 51;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "1";
            this.groupBox8.Enter += new System.EventHandler(this.groupBox8_Enter);
            // 
            // comboBox_UTF_Type
            // 
            this.comboBox_UTF_Type.FormattingEnabled = true;
            this.comboBox_UTF_Type.Items.AddRange(new object[] {
            "UTF8",
            "UTF7",
            "Unicode"});
            this.comboBox_UTF_Type.Location = new System.Drawing.Point(239, 99);
            this.comboBox_UTF_Type.Name = "comboBox_UTF_Type";
            this.comboBox_UTF_Type.Size = new System.Drawing.Size(138, 21);
            this.comboBox_UTF_Type.TabIndex = 53;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(264, 81);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(112, 13);
            this.label40.TabIndex = 52;
            this.label40.Text = "Select Encoding Type";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 51;
            this.label18.Text = "Status:";
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(9, 99);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(165, 23);
            this.button15.TabIndex = 50;
            this.button15.Text = "Load SOSI and  Assign ID";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(405, 515);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "NEW Mapping";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.button11);
            this.groupBox6.Controls.Add(this.progressBar8);
            this.groupBox6.Controls.Add(this.progressBar7);
            this.groupBox6.Location = new System.Drawing.Point(13, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(383, 103);
            this.groupBox6.TabIndex = 46;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "1:Surface generaction";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 54);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "Status:";
            // 
            // progressBar8
            // 
            this.progressBar8.Location = new System.Drawing.Point(10, 19);
            this.progressBar8.Name = "progressBar8";
            this.progressBar8.Size = new System.Drawing.Size(364, 11);
            this.progressBar8.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar8.TabIndex = 45;
            // 
            // progressBar7
            // 
            this.progressBar7.Location = new System.Drawing.Point(11, 36);
            this.progressBar7.Name = "progressBar7";
            this.progressBar7.Size = new System.Drawing.Size(363, 15);
            this.progressBar7.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar7.TabIndex = 44;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox_KPBuilding2);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.dateTimePicker2);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.progressBar6);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.Txt_REGISTRERINGSVERSJON_new);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.combox_FKB_new);
            this.groupBox2.Controls.Add(this.button22);
            this.groupBox2.Controls.Add(this.button23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Location = new System.Drawing.Point(12, 252);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(384, 257);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "3:Final SOSI conversion";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(12, 133);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(119, 17);
            this.checkBox3.TabIndex = 57;
            this.checkBox3.Text = "Enter Date of Flying";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(169, 156);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(148, 20);
            this.textBox13.TabIndex = 56;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(169, 130);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(206, 20);
            this.dateTimePicker2.TabIndex = 55;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 206);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 54;
            this.label26.Text = "Status:";
            // 
            // progressBar6
            // 
            this.progressBar6.Location = new System.Drawing.Point(12, 182);
            this.progressBar6.Name = "progressBar6";
            this.progressBar6.Size = new System.Drawing.Size(363, 21);
            this.progressBar6.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar6.TabIndex = 53;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "Select Output folder";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 106);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(141, 13);
            this.label23.TabIndex = 47;
            this.label23.Text = "Select sosi for Date of Flying";
            // 
            // Txt_REGISTRERINGSVERSJON_new
            // 
            this.Txt_REGISTRERINGSVERSJON_new.Location = new System.Drawing.Point(169, 74);
            this.Txt_REGISTRERINGSVERSJON_new.Name = "Txt_REGISTRERINGSVERSJON_new";
            this.Txt_REGISTRERINGSVERSJON_new.Size = new System.Drawing.Size(149, 20);
            this.Txt_REGISTRERINGSVERSJON_new.TabIndex = 48;
            this.Txt_REGISTRERINGSVERSJON_new.Text = "FKB 4.02";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(170, 101);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(149, 20);
            this.textBox8.TabIndex = 51;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(113, 20);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(207, 20);
            this.textBox9.TabIndex = 43;
            // 
            // combox_FKB_new
            // 
            this.combox_FKB_new.FormattingEnabled = true;
            this.combox_FKB_new.Items.AddRange(new object[] {
            "FKB-A",
            "FKB-B",
            "FKB-C"});
            this.combox_FKB_new.Location = new System.Drawing.Point(169, 48);
            this.combox_FKB_new.Name = "combox_FKB_new";
            this.combox_FKB_new.Size = new System.Drawing.Size(149, 21);
            this.combox_FKB_new.TabIndex = 50;
            this.combox_FKB_new.Text = "FKB-A";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(328, 18);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(50, 23);
            this.button22.TabIndex = 45;
            this.button22.Text = "Browse";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(327, 98);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(50, 23);
            this.button23.TabIndex = 52;
            this.button23.Text = "Browse";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 13);
            this.label22.TabIndex = 49;
            this.label22.Text = "Registration version";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "FKB Standard";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Controls.Add(this.button17);
            this.tabPage5.Controls.Add(this.button16);
            this.tabPage5.Controls.Add(this.checkedListBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(405, 515);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "SPLIT FILE";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.button19);
            this.groupBox4.Controls.Add(this.textBox7);
            this.groupBox4.Controls.Add(this.button18);
            this.groupBox4.Controls.Add(this.progressBar5);
            this.groupBox4.Location = new System.Drawing.Point(12, 370);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(376, 115);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Validate DGN DATA";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 58;
            this.label19.Text = "Select DGN file";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(301, 17);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(69, 23);
            this.button19.TabIndex = 57;
            this.button19.Text = "Browse";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(92, 20);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(201, 20);
            this.textBox7.TabIndex = 56;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(6, 81);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 55;
            this.button18.Text = "Validate";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // progressBar5
            // 
            this.progressBar5.Location = new System.Drawing.Point(7, 57);
            this.progressBar5.Name = "progressBar5";
            this.progressBar5.Size = new System.Drawing.Size(363, 18);
            this.progressBar5.TabIndex = 54;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(313, 307);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 53;
            this.button17.Text = "Split Files";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(12, 307);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 52;
            this.button16.Text = "List Files";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(12, 12);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(376, 289);
            this.checkedListBox1.TabIndex = 51;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.groupBox15);
            this.tabPage1.Controls.Add(this.button32);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.radioButton4);
            this.tabPage1.Controls.Add(this.radioButton3);
            this.tabPage1.Controls.Add(this.checkedListBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(405, 515);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "SETTING/TOOLS";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(10, 3);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(75, 13);
            this.label39.TabIndex = 59;
            this.label39.Text = "Surface Name";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button28);
            this.groupBox15.Controls.Add(this.button25);
            this.groupBox15.Controls.Add(this.button26);
            this.groupBox15.Controls.Add(this.button27);
            this.groupBox15.Location = new System.Drawing.Point(166, 326);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(231, 135);
            this.groupBox15.TabIndex = 58;
            this.groupBox15.TabStop = false;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(35, 19);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(145, 23);
            this.button28.TabIndex = 55;
            this.button28.Text = "Assign VBase Attribute";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(35, 106);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(145, 23);
            this.button25.TabIndex = 52;
            this.button25.Text = "Highlight Surface";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(35, 77);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(145, 23);
            this.button26.TabIndex = 53;
            this.button26.Text = "Surface From Fence";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(35, 48);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(145, 23);
            this.button27.TabIndex = 54;
            this.button27.Text = "Surface From Selection";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(13, 415);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(144, 23);
            this.button32.TabIndex = 57;
            this.button32.Text = "Off Unused Surface Layer";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button29);
            this.groupBox5.Controls.Add(this.Validation6);
            this.groupBox5.Controls.Add(this.progressBar9);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.Validation5);
            this.groupBox5.Controls.Add(this.Validation4);
            this.groupBox5.Controls.Add(this.StartValidatin);
            this.groupBox5.Controls.Add(this.button31);
            this.groupBox5.Controls.Add(this.Validation3);
            this.groupBox5.Controls.Add(this.Validation2);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.Validation1);
            this.groupBox5.Controls.Add(this.richTextBox2);
            this.groupBox5.Location = new System.Drawing.Point(163, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(234, 317);
            this.groupBox5.TabIndex = 56;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Validations";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(166, 61);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(60, 23);
            this.button29.TabIndex = 59;
            this.button29.Text = "Browse";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // Validation6
            // 
            this.Validation6.AutoSize = true;
            this.Validation6.Location = new System.Drawing.Point(146, 141);
            this.Validation6.Name = "Validation6";
            this.Validation6.Size = new System.Drawing.Size(80, 17);
            this.Validation6.TabIndex = 9;
            this.Validation6.TabStop = true;
            this.Validation6.Text = "Validation-6";
            this.Validation6.UseVisualStyleBackColor = true;
            this.Validation6.CheckedChanged += new System.EventHandler(this.Validation6_CheckedChanged);
            // 
            // progressBar9
            // 
            this.progressBar9.Location = new System.Drawing.Point(6, 19);
            this.progressBar9.Name = "progressBar9";
            this.progressBar9.Size = new System.Drawing.Size(220, 10);
            this.progressBar9.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar9.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 58;
            this.label24.Text = "Select SOSI file";
            // 
            // Validation5
            // 
            this.Validation5.AutoSize = true;
            this.Validation5.Location = new System.Drawing.Point(146, 118);
            this.Validation5.Name = "Validation5";
            this.Validation5.Size = new System.Drawing.Size(80, 17);
            this.Validation5.TabIndex = 8;
            this.Validation5.TabStop = true;
            this.Validation5.Text = "Validation-5";
            this.Validation5.UseVisualStyleBackColor = true;
            this.Validation5.CheckedChanged += new System.EventHandler(this.Validation5_CheckedChanged);
            // 
            // Validation4
            // 
            this.Validation4.AutoSize = true;
            this.Validation4.Location = new System.Drawing.Point(146, 95);
            this.Validation4.Name = "Validation4";
            this.Validation4.Size = new System.Drawing.Size(80, 17);
            this.Validation4.TabIndex = 7;
            this.Validation4.TabStop = true;
            this.Validation4.Text = "Validation-4";
            this.Validation4.UseVisualStyleBackColor = true;
            this.Validation4.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // StartValidatin
            // 
            this.StartValidatin.Location = new System.Drawing.Point(38, 164);
            this.StartValidatin.Name = "StartValidatin";
            this.StartValidatin.Size = new System.Drawing.Size(145, 23);
            this.StartValidatin.TabIndex = 6;
            this.StartValidatin.Text = "Start Validation";
            this.StartValidatin.UseVisualStyleBackColor = true;
            this.StartValidatin.Click += new System.EventHandler(this.StartValidatin_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(6, 61);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(62, 23);
            this.button31.TabIndex = 4;
            this.button31.Text = "Load File";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // Validation3
            // 
            this.Validation3.AutoSize = true;
            this.Validation3.Location = new System.Drawing.Point(6, 141);
            this.Validation3.Name = "Validation3";
            this.Validation3.Size = new System.Drawing.Size(80, 17);
            this.Validation3.TabIndex = 5;
            this.Validation3.TabStop = true;
            this.Validation3.Text = "Validation-3";
            this.Validation3.UseVisualStyleBackColor = true;
            this.Validation3.CheckedChanged += new System.EventHandler(this.Validation3_CheckedChanged);
            // 
            // Validation2
            // 
            this.Validation2.AutoSize = true;
            this.Validation2.Location = new System.Drawing.Point(6, 118);
            this.Validation2.Name = "Validation2";
            this.Validation2.Size = new System.Drawing.Size(80, 17);
            this.Validation2.TabIndex = 4;
            this.Validation2.TabStop = true;
            this.Validation2.Text = "Validation-2";
            this.Validation2.UseVisualStyleBackColor = true;
            this.Validation2.CheckedChanged += new System.EventHandler(this.Validation2_CheckedChanged);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(90, 35);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(136, 20);
            this.textBox10.TabIndex = 3;
            // 
            // Validation1
            // 
            this.Validation1.AutoSize = true;
            this.Validation1.Location = new System.Drawing.Point(6, 95);
            this.Validation1.Name = "Validation1";
            this.Validation1.Size = new System.Drawing.Size(80, 17);
            this.Validation1.TabIndex = 3;
            this.Validation1.TabStop = true;
            this.Validation1.Text = "Validation-1";
            this.Validation1.UseVisualStyleBackColor = true;
            this.Validation1.CheckedChanged += new System.EventHandler(this.Validation1_CheckedChanged);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(6, 204);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(222, 98);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(13, 470);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(81, 17);
            this.radioButton4.TabIndex = 2;
            this.radioButton4.Text = "Unselect All";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(13, 444);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(75, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.Text = "Selcect All";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.CheckOnClick = true;
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Location = new System.Drawing.Point(13, 30);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(144, 379);
            this.checkedListBox2.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(405, 515);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "ORTHO";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox14);
            this.groupBox7.Controls.Add(this.groupBox13);
            this.groupBox7.Controls.Add(this.groupBox12);
            this.groupBox7.Controls.Add(this.groupBox11);
            this.groupBox7.Location = new System.Drawing.Point(8, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(394, 508);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.progressBar12);
            this.groupBox14.Controls.Add(this.OrthoSurface);
            this.groupBox14.Location = new System.Drawing.Point(6, 11);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(381, 66);
            this.groupBox14.TabIndex = 53;
            this.groupBox14.TabStop = false;
            // 
            // progressBar12
            // 
            this.progressBar12.Location = new System.Drawing.Point(5, 16);
            this.progressBar12.Name = "progressBar12";
            this.progressBar12.Size = new System.Drawing.Size(369, 10);
            this.progressBar12.TabIndex = 26;
            // 
            // OrthoSurface
            // 
            this.OrthoSurface.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrthoSurface.Location = new System.Drawing.Point(9, 36);
            this.OrthoSurface.Name = "OrthoSurface";
            this.OrthoSurface.Size = new System.Drawing.Size(158, 23);
            this.OrthoSurface.TabIndex = 25;
            this.OrthoSurface.Text = "Assign Surface Attribute";
            this.OrthoSurface.UseVisualStyleBackColor = true;
            this.OrthoSurface.Click += new System.EventHandler(this.OrthoSurface_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label36);
            this.groupBox13.Controls.Add(this.label37);
            this.groupBox13.Controls.Add(this.button36);
            this.groupBox13.Controls.Add(this.textBox11);
            this.groupBox13.Controls.Add(this.progressBar11);
            this.groupBox13.Controls.Add(this.button37);
            this.groupBox13.Location = new System.Drawing.Point(3, 83);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(385, 94);
            this.groupBox13.TabIndex = 52;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "1";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(176, 73);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(40, 13);
            this.label36.TabIndex = 51;
            this.label36.Text = "Status:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 25);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(122, 13);
            this.label37.TabIndex = 40;
            this.label37.Text = "Select Original SOSI File";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(329, 20);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(50, 23);
            this.button36.TabIndex = 39;
            this.button36.Text = "Browse";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(136, 22);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(185, 20);
            this.textBox11.TabIndex = 41;
            // 
            // progressBar11
            // 
            this.progressBar11.Location = new System.Drawing.Point(10, 48);
            this.progressBar11.Name = "progressBar11";
            this.progressBar11.Size = new System.Drawing.Size(370, 10);
            this.progressBar11.TabIndex = 49;
            // 
            // button37
            // 
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.Location = new System.Drawing.Point(12, 63);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(158, 23);
            this.button37.TabIndex = 50;
            this.button37.Text = "Load SOSI";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label33);
            this.groupBox12.Controls.Add(this.progressBar10);
            this.groupBox12.Controls.Add(this.button33);
            this.groupBox12.Controls.Add(this.label34);
            this.groupBox12.Controls.Add(this.label35);
            this.groupBox12.Controls.Add(this.Ipbox8);
            this.groupBox12.Controls.Add(this.Ipbox7);
            this.groupBox12.Controls.Add(this.button34);
            this.groupBox12.Controls.Add(this.button35);
            this.groupBox12.Location = new System.Drawing.Point(2, 367);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(386, 135);
            this.groupBox12.TabIndex = 44;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "3";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(10, 82);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 54;
            this.label33.Text = "Status:";
            // 
            // progressBar10
            // 
            this.progressBar10.Location = new System.Drawing.Point(9, 66);
            this.progressBar10.Name = "progressBar10";
            this.progressBar10.Size = new System.Drawing.Size(370, 13);
            this.progressBar10.TabIndex = 53;
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.Location = new System.Drawing.Point(9, 108);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(162, 23);
            this.button33.TabIndex = 0;
            this.button33.Text = "Create SOSI File";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "Select Output folder";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 45);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(98, 13);
            this.label35.TabIndex = 47;
            this.label35.Text = "Default attribute file";
            // 
            // Ipbox8
            // 
            this.Ipbox8.Location = new System.Drawing.Point(113, 40);
            this.Ipbox8.Name = "Ipbox8";
            this.Ipbox8.Size = new System.Drawing.Size(207, 20);
            this.Ipbox8.TabIndex = 51;
            // 
            // Ipbox7
            // 
            this.Ipbox7.Location = new System.Drawing.Point(113, 11);
            this.Ipbox7.Name = "Ipbox7";
            this.Ipbox7.Size = new System.Drawing.Size(207, 20);
            this.Ipbox7.TabIndex = 43;
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(328, 9);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(50, 23);
            this.button34.TabIndex = 45;
            this.button34.Text = "Browse";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(328, 37);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(50, 23);
            this.button35.TabIndex = 52;
            this.button35.Text = "Browse";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.Ipbox6);
            this.groupBox11.Controls.Add(this.Ipbox2);
            this.groupBox11.Controls.Add(this.Ipbox1);
            this.groupBox11.Controls.Add(this.Ipbox5);
            this.groupBox11.Controls.Add(this.Ipbox4);
            this.groupBox11.Controls.Add(this.label27);
            this.groupBox11.Controls.Add(this.Ipbox3);
            this.groupBox11.Controls.Add(this.label28);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Location = new System.Drawing.Point(3, 185);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(387, 185);
            this.groupBox11.TabIndex = 26;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "2:  SOSI Header Input";
            // 
            // Ipbox6
            // 
            this.Ipbox6.Location = new System.Drawing.Point(165, 159);
            this.Ipbox6.Name = "Ipbox6";
            this.Ipbox6.Size = new System.Drawing.Size(118, 20);
            this.Ipbox6.TabIndex = 24;
            // 
            // Ipbox2
            // 
            this.Ipbox2.Location = new System.Drawing.Point(165, 51);
            this.Ipbox2.Name = "Ipbox2";
            this.Ipbox2.Size = new System.Drawing.Size(118, 20);
            this.Ipbox2.TabIndex = 23;
            this.Ipbox2.Text = "4.0";
            // 
            // Ipbox1
            // 
            this.Ipbox1.Location = new System.Drawing.Point(165, 24);
            this.Ipbox1.Name = "Ipbox1";
            this.Ipbox1.Size = new System.Drawing.Size(118, 20);
            this.Ipbox1.TabIndex = 8;
            this.Ipbox1.Text = "22";
            // 
            // Ipbox5
            // 
            this.Ipbox5.Location = new System.Drawing.Point(165, 133);
            this.Ipbox5.Name = "Ipbox5";
            this.Ipbox5.Size = new System.Drawing.Size(118, 20);
            this.Ipbox5.TabIndex = 22;
            this.Ipbox5.Text = "GEOVEKST";
            // 
            // Ipbox4
            // 
            this.Ipbox4.Location = new System.Drawing.Point(165, 107);
            this.Ipbox4.Name = "Ipbox4";
            this.Ipbox4.Size = new System.Drawing.Size(118, 20);
            this.Ipbox4.TabIndex = 21;
            this.Ipbox4.Text = "COWI AS";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 159);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(88, 13);
            this.label27.TabIndex = 20;
            this.label27.Text = "PRODUKTSPEK";
            // 
            // Ipbox3
            // 
            this.Ipbox3.Location = new System.Drawing.Point(165, 81);
            this.Ipbox3.Name = "Ipbox3";
            this.Ipbox3.Size = new System.Drawing.Size(118, 20);
            this.Ipbox3.TabIndex = 11;
            this.Ipbox3.Text = "4";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 136);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 13);
            this.label28.TabIndex = 19;
            this.label28.Text = "EIER";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 13;
            this.label29.Text = "KOORDSYS";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(5, 110);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 13);
            this.label30.TabIndex = 18;
            this.label30.Text = "PRODUSENT";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 84);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 13);
            this.label31.TabIndex = 17;
            this.label31.Text = "SOSI-NIVÅ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 54);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(85, 13);
            this.label32.TabIndex = 16;
            this.label32.Text = "SOSI-VERSJON";
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(462, 233);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(102, 23);
            this.button24.TabIndex = 3;
            this.button24.Text = "Point Inside Test";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(462, 171);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 1;
            this.button20.Text = "List All Surfcse";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(462, 200);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 3;
            this.button21.Text = "button21";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(478, 404);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(75, 23);
            this.button30.TabIndex = 52;
            this.button30.Text = "button30";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // checkBox_KPBuilding1
            // 
            this.checkBox_KPBuilding1.AutoSize = true;
            this.checkBox_KPBuilding1.Location = new System.Drawing.Point(239, 225);
            this.checkBox_KPBuilding1.Name = "checkBox_KPBuilding1";
            this.checkBox_KPBuilding1.Size = new System.Drawing.Size(127, 17);
            this.checkBox_KPBuilding1.TabIndex = 43;
            this.checkBox_KPBuilding1.Text = "Use Building KP Rule";
            this.checkBox_KPBuilding1.UseVisualStyleBackColor = true;
            // 
            // checkBox_KPBuilding2
            // 
            this.checkBox_KPBuilding2.AutoSize = true;
            this.checkBox_KPBuilding2.Location = new System.Drawing.Point(253, 228);
            this.checkBox_KPBuilding2.Name = "checkBox_KPBuilding2";
            this.checkBox_KPBuilding2.Size = new System.Drawing.Size(127, 17);
            this.checkBox_KPBuilding2.TabIndex = 58;
            this.checkBox_KPBuilding2.Text = "Use Building KP Rule";
            this.checkBox_KPBuilding2.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 541);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.Txt_TEGNSETT);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_ENHET);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "CIL:DGN To SOSI  V1.00.00.04   D201604028-1";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox Txt_TEGNSETT;
        private System.Windows.Forms.TextBox Txt_KOORDSYS;
        private System.Windows.Forms.TextBox Txt_ENHET;
        private System.Windows.Forms.TextBox Txt_SOSI_NIVA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LBL_PRODUKTSPEK;
        private System.Windows.Forms.TextBox Txt_PRODUSENT;
        private System.Windows.Forms.TextBox Txt_EIER;
        private System.Windows.Forms.TextBox Txt_SOSI_VERSJON;
        private System.Windows.Forms.TextBox Txt_PRODUKTSPEK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_REGISTRERINGSVERSJON;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox combox_FKB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.ProgressBar progressBar5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar progressBar6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Txt_REGISTRERINGSVERSJON_new;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.ComboBox combox_FKB_new;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.ProgressBar progressBar7;
        private System.Windows.Forms.ProgressBar progressBar8;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.ProgressBar progressBar9;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button StartValidatin;
        private System.Windows.Forms.RadioButton Validation3;
        private System.Windows.Forms.RadioButton Validation2;
        private System.Windows.Forms.RadioButton Validation1;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.RadioButton Validation4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ProgressBar progressBar10;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox Ipbox8;
        private System.Windows.Forms.TextBox Ipbox7;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox Ipbox6;
        private System.Windows.Forms.TextBox Ipbox2;
        private System.Windows.Forms.TextBox Ipbox1;
        private System.Windows.Forms.TextBox Ipbox5;
        private System.Windows.Forms.TextBox Ipbox4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Ipbox3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ProgressBar progressBar11;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button OrthoSurface;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ProgressBar progressBar12;
        private System.Windows.Forms.RadioButton Validation5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.RadioButton Validation6;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBox_UTF_Type;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox checkBox_KPBuilding1;
        private System.Windows.Forms.CheckBox checkBox_KPBuilding2;
    }
}